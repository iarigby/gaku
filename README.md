# Gaku

ELTE tools of software engineering group 4

This is the main repository containing the API of the back end. Front end can be found here https://gitlab.com/silentsurvivor/gaku-front-end/

# Development
## Installation
1. Follow your OS instructions for installing npm
2. Clone this repo and in its directory, run `npm install` and it will download and set up all of the dependencies

## Running
### Nodemon
Run `npm start` to start the server using nodemon, so the changes in the editor will be automatically reloaded
Please make sure you are running MongoDB first, we are using a MongoDB Docker container during development.
Check the Tests section for instructions on setting MongoDB up.

### healthCheck endpoint
After starting the server, go to `localhost:3000/healthCheck` to make sure everything is working properly. Your request will be logged too.

### Tests
Tests can be executed with `npm test`.
We are using a Docker MongoDB instance for db tests. Install Docker according your OS instructions and start it.
Next, pull the official MongoDB image `docker pull mongo`.
To start the container: `docker run --rm --name test-db -p 27017:27017 mongo`. The --rm flag ensures the container is cleaned up nicely after testing.
You should now be able to execute the tests with `npm test`

## Documentation & Swagger
This tool helps visualize the architecture using documentation. You can see the examples in the codebase, or read about full features in the [documentation](https://github.com/pgroot/express-swagger-generator/).
To access the ui, visit `localhost:3000/api-docs`

# Project Management
## working on issues
1. Make sure to move the issue to doing list on [the board](https://gitlab.com/iarigby/gaku/boards)
2. From the issue page, click the `create merge request` button. It will automatically create the branch that you can use for developing

## commit messages
! keep commit content short, messages clear, files organized !
* [ ]  `TODO` agree on commit message standard (eg including issue number)

first line should include summary. Provide more details on following lines (start lines with `-` so it can be displayed as a list)

## `TODO` merge request description
* [ ] decide if we're using squash commits
