const options = {
  swaggerDefinition: {
    info: {
      description: ' ',
      title: 'Gaku',
      version: '1.0.0',
    },
    host: 'localhost:3000',
    basePath: '/v1',
    produces: [
      'application/json',
      'application/xml'
    ],
    schemes: ['http', 'https'],
    securityDefinitions: {
      JWT: {
        type: 'apiKey',
        in: 'header',
        name: 'Authorization',
        description: '',
      }
    }
  },
  basedir: __dirname, //app absolute path
  files: ['../api/routes/*.js', '../models/**/*.js'] //Path to the API handle folder
}

module.exports = options
