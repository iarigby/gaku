const mongoose = require('mongoose')
mongoose.set('useCreateIndex', true) // avoid depracated warning

if (process.env.MONGODB_URI) { // attempt to connect to the URI we defined first
  mongoose.connect(process.env.MONGODB_URI, {useNewUrlParser: true})
} else {
  const db = 'mongodb://localhost/testDatabase'
  mongoose.connect(db, {useNewUrlParser: true},
    function (err) {
      if (err) {
        console.log(err)
      } else {
        console.log('MongoDB host not configured, connecting to default')
        console.log('mongoose connection is successful on: ' + db)
      }
    })
}
