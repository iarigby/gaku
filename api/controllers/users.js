const User = require('../../models/user').model

exports.create_user = (req, res, next) => {
  const newUser = User(req.body)
  // TODO maybe check with spotify that
  // auth that front end is sending is valid
  newUser.validate()
  newUser.save()
    .then(() =>
      res.status(201).json({newUser})
    )
    .catch(err => res.json(err))
}

exports.get_user = (req, res, next) => {
  User.findById(req.params.id, function (err, user) {
    if (!user) {
      res.status(404)
    }
    res.json({user})
  })
}
