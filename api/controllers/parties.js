const Party = require('../../models/party').model
const QueueEntry = require('../../models/queue.entry').model

exports.create_party = (req, res, next) => {
  const newParty = Party(req.body)
  // TODO maybe check with spotify that
  // auth that front end is sending is valid
  newParty.validate()
  newParty.save()
    .then(() =>
      res.status(201).json({newParty})
    )
    .catch(err => res.json(err))
}

exports.get_party = (req, res, next) => {
  Party.findById(req.params.id, function (err, party) {
    if (!party) {
      res.status(404)
    }
    res.json({party})
  })
}

exports.submit_vote = (req, res, next) => {
  Party.findById(req.params.id, function (err, party) {
    if (!party) {
      return res.status(404) //trying to submit a vote to non-existing party
    }
    for (var i = 0; i < party.queue.length; i++) { //search if queue entry already exists so we just submit a new vote
      var queueEntry = party.queue[i]
      if (queueEntry.track._id === req.body.track_id) {
        QueueEntry.update(
          {_id : queueEntry._id},
          {$addToSet : {votes: req.body.user_id}}) //addToSet insures there wont be duplicate elements
        return res.status(201).json({queueEntry})
      }
    }
    var newQueueEntry =  QueueEntry({ //if queue entry doesn't exist we create one
      track : req.body.track_id,
      votes : [req.body.user_id]
    })
    Party.update(
      {_id : req.params.id},
      {$push : {queue: newQueueEntry}})
    return res.status(201).json({newQueueEntry})
  })
}
