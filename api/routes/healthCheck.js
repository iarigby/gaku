/**
 * healthCheck endpoint to check if the server is running
 */

const express = require('express')
const router = express.Router()

/**
 * @typedef Error
 * @property {number} code.required
 * @property {string} description
 */

// this refers to '/healthCheck'
// since app.js forwards all the requests on that route to here
router.get('/', (req, res, next) => {
  res.status(200).json({
    status: 'ok'
  })
})

module.exports = router
