const router = require('express').Router()
const UserController = require('../controllers/users')

/**
 * create new user. Parameters need to be provided in body, encoded in JSON
 * @route POST /users/
 * @group user - operations about the user
 * @param {string} spotify_auth - token  received from spotify at authentication
 * @param {Profile.model} profile - info from spotify
 * @returns {User.model} 201 - successfully created new user
 */
router.post('/', UserController.create_user)

/**
 * get all information about the user
 * @route GET /users/:user
 * @group user
 * @param {string} id - user id
 * @returns {User.model} 200
 * @returns {Error} 404 - user not found
 */
router.get('/:id', UserController.get_user)

module.exports = router
