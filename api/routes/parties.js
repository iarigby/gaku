const router = require('express').Router()
const PartyController = require('../controllers/parties')

/**
 * create a new party. Parameters need to be provided in body, encoded in JSON
 * @route POST /parties
 * @group party - operations about the parties
 * @param {string} name - party name, needs to be unique
 * @param {string} host - id of host
 * @returns {Party.model} 201 - successfully created new party
 * @returns {Error} 404 - party not found
 */
router.post('/', /*auth.spotify,*/ PartyController.create_party)

/**
 * get the info about the party by name
 * @route GET /parties/:id
 * @group party
 * @returns {Party.model} 200 - all info you might need about the party
 */
router.get('/:id', /*auth.spotify,*/ PartyController.get_party)

/**
 * submit a vote to a song of a party
 * @route POST /parties/:id/vote
 * @group party
 * @param {string} song - id of the song
 * @param {string} user - id of the user who's submitting the vote
 * @returns 201 - successfully submitted the vote
 * @returns {Error} 404 - party not found
 */
router.post('/:id/vote', /*auth.spotify,*/ PartyController.submit_vote)

module.exports = router
