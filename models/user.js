const mongoose = require('mongoose')
const Schema = mongoose.Schema
const ProfileSchema = require('./spotify/profile').schema
/**
 * @typedef User
 * @property {Profile.model} profile all of the info of the user from spotify
 * @property {string} spotify_auth Oauth token that we receive after user authenticates the profiel
 * @property {Array.<string>} hosted_parties ids of parties they have created
 * @property {Array.<string>} visited_parties ids of parties they have voted on
 */
const UserSchema = new Schema({
  profile: {type: ProfileSchema, required: true},
  spotify_auth: {type: String, required: true},
  hosted_parties: [{type: Schema.Types.ObjectId, ref: 'Party'}],
  visited_parties: [{type: Schema.Types.ObjectId, ref: 'Party'}]
})

//UserSchema.methods.toAuthJSON = function () {
//  return {
//    profile: this.profile,
//    spotify_auth: this.spotify_auth,
//    hosted_parties: this.hosted_parties,
//    visited_parties: this.visited_parties,
//  }
//}

module.exports = {
  model: mongoose.model('User', UserSchema),
  schema: UserSchema,
}
