const mongoose = require('mongoose')
const Schema = mongoose.Schema
const QueueEntry = require('./queue.entry').schema


/**
 * @typedef Party
 * @property {string} name.unique also used as uuid
 * @property {string} host id for reference
 * @property {string} party_playlist  database id to spotify playlist
 * @property {string} now_playing id of the track that is now playing
 * (other info about the track is in the queue, no need to use populate())
 * @property {Array.<QueueEntry>} queue
 */
const PartySchema = new Schema({
  name: {type: String, required: true, unique: true},
  host: {type: Schema.Types.ObjectId, ref: 'User'},
  party_playlist: {type: Schema.Types.ObjectId, ref: 'Playlist'},
  now_playling: {type: Schema.Types.ObjectId, ref: 'Track'},
  queue: [QueueEntry]
})

module.exports = {
  model: mongoose.model('Party', PartySchema),
  schema: PartySchema,
}
