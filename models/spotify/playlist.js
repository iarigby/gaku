const mongoose = require('mongoose')
const Schema = mongoose.Schema

/**
 * @typedef Playlist - we only really need to remember the playlist_id
 * @property {string} playlist_name the name of the playlist
 * @property {string} playlist_id the Spotify ID for the playlist
 */
const PlaylistSchema = new Schema({
  name: {type: String, required: true},
  id: {type: String, required: true}
})

module.exports = {
  model: mongoose.model('Playlist', PlaylistSchema),
  schema: PlaylistSchema,
}
