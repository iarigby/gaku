const mongoose = require('mongoose')
const Schema = mongoose.Schema

/**
 * @typedef Profile info, fields are a subset from Spotify API
 * @property {string} display_name the name displayed on the user’s profile. null if not available
 * @property {string} href a link to the Web API endpoint for this user
 * @property {string} id Spotify user ID for this user
 * @property {string} images the user’s profile image
 * @property {string} type the object type: “user”
 * @property {string} uri the Spotify URI for this user
 */
const ProfileSchema = new Schema({
  display_name: {type: String, required: true},
  href: String,
  id: String,
  images: [{
    height: Number,
    url: String,
    width: Number
  }],
  type: String,
  uri: String
})

module.exports = {
  model: mongoose.model('Profile', ProfileSchema),
  schema: ProfileSchema,
}
