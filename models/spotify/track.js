const mongoose = require('mongoose')
const Schema = mongoose.Schema

/**
 * @typedef Track - we only really should need to remember the id
 * @property {string} track_name name of track
 * @property {string} spotify_id this is used to reference the track with the Spotify API
 */
const TrackSchema = new Schema({
  name: {type: String, required: true},
  spotify_id: {type: String, required: true}
})

module.exports = {
  model: mongoose.model('Track', TrackSchema),
  schema: TrackSchema,
}
