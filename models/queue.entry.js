const mongoose = require('mongoose')
const Schema = mongoose.Schema
const TrackSchema = require('./spotify/track').schema

/**
 * @typedef QueueEntry
 * @property {Track.model} track
 * @property {Array.<string>} votes - ids of users that submitted a vote to this track
 */
const QueueEntrySchema = new Schema({
  track: {type: TrackSchema, required: true},
  votes: [{type: Schema.Types.ObjectId, ref: 'User'}]
})


module.exports = {
  model: mongoose.model('QueueEntry', QueueEntrySchema),
  schema: QueueEntrySchema,
}
