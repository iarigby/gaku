const Profile = require('../../models/spotify/profile').model
const User = require('../../models/user').model

module.exports.validProfile = function() {
  return Profile({
    display_name : 'Lilla Namo',
    href : 'https://api.spotify.com/v1/users/tuggareutangranser',
    id : 'tuggareutangranser',
    images : [{
      height : null,
      url : 'http://profile-images.scdn.co/artists/default/d4f208d4d49c6f3e1363765597d10c4277f5b74f',
      width : null
    }],
    type : 'user',
    uri : 'spotify:user:tuggareutangranser'
  })
}

module.exports.invalidProfile = function() {
  return Profile({
    NotDisplay_name : 123,
    href : 123,
    id : 123,
    images : [{
      height : null,
      url : 123,
      width : null
    }],
    type : 'user',
    uri : 'spotify:user:tuggareutangranser'
  })
}
