const QueueEntry = require('../../models/queue.entry').model
const trackFactory = require('./spotify/track.factory')

module.exports.validQueueEntry = function() {

  return  QueueEntry({
    track : trackFactory.validTrack(),
    votes : []
  })
}

