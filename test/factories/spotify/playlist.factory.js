const Playlist = require('../../../models/spotify/playlist').model

const randomstring = require('randomstring')

module.exports.validPlaylist = function() {
  return Playlist({
    name : randomstring.generate(),
    id:randomstring.generate()
  })
}
