const Track = require('../../../models/spotify/track').model
const randomstring = require('randomstring')

module.exports.validTrack = function() {
  return Track({
    name : randomstring.generate(),
    spotify_id : randomstring.generate()
  })
}
