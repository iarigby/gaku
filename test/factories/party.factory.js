const Party = require('../../models/party').model
const userFactory = require('./user.factory')
const queueEntryFactory = require('./queue.entry.factory')

const randomstring = require('randomstring')

module.exports.validParty = function() {
  return  Party({
    name : randomstring.generate(),
    host : userFactory.validUser()._id,
    queue: [queueEntryFactory.validQueueEntry()]
  })
}
