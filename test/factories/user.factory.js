const User = require('../../models/user').model
const profileFactory = require('./profile.factory')

module.exports.validUser = function() {
  return User({
    profile : profileFactory.validProfile(),
    spotify_auth : 'randomSpotifyAuthString'
  })
}
