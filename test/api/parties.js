const app = require('../../app')

const chai = require('chai')
const chaiHttp = require('chai-http')
const should = chai.should()
const expect = chai.expect

chai.use(chaiHttp)
const request = chai.request(app)

const partyFactory = require('../factories/party.factory')
const trackFactory = require('../factories/spotify/track.factory')
const userFactory = require('../factories/user.factory')


describe('Parties', () => {
  const validParty = partyFactory.validParty()

  describe('POST /', () => {
    it('should add a party', (done) => {
      chai.request(app)
        .post('/parties')
        .send(validParty)
        .end((err, res) => {
          res.should.have.status(201)
          res.body.should.be.a('object')
          expect(res.body.newParty._id).to.equal(validParty.id)
          done()
        })
    })

    const votingUser = userFactory.validUser()

    it('should submit a vote for existing queue entry', (done) => {
      const voteRequestBody = {
        track_id: validParty.queue[0].track._id,
        user_id:  votingUser.id
      }
      chai.request(app)
        .post(`/parties/${validParty.id}/vote`)
        .send(voteRequestBody)
        .end((err, res) => {
          res.should.have.status(201)
          res.body.should.be.a('object')
          done()
        })
    })

    it('should create a new queue entry by submiting a vote', (done) => {
      const trackForVote = trackFactory.validTrack()
      const voteRequestBody = {
        track_id: trackForVote.id,
        user_id:  votingUser.id
      }
      chai.request(app)
        .post(`/parties/${validParty.id}/vote`)
        .send(voteRequestBody)
        .end((err, res) => {
          res.should.have.status(201)
          res.body.should.be.a('object')
          done()
        })
    })

  })
  describe('GET /', () => {
    it('should get a single party record', (done) => {
      chai.request(app)
        .get(`/parties/${validParty.id}`)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          expect(res.body.party._id).to.equal(validParty.id)
          done()
        })
    })

    it('should not get a single party record', (done) => {
      const id = 5 //This should be an example for an entry which does not exist yet
      chai.request(app)
        .get(`/parties/${id}`)
        .end((err, res) => {
          res.should.have.status(404)
          done()
        })
    })
  })
})
