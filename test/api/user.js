const app = require('../../app')

const chai = require('chai')
const chaiHttp = require('chai-http')
const should = chai.should()
const expect = chai.expect

chai.use(chaiHttp)
const request = chai.request(app)

const userFactory = require('../factories/user.factory')


describe('Users', () => {
  const validUser = userFactory.validUser()
  describe('POST /', () => {
    it('should add a user', (done) => {
      chai.request(app)
        .post('/users')
        .send(validUser)
        .end((err, res) => {
          res.should.have.status(201)
          res.body.should.be.a('object')
          expect(res.body.newUser._id).to.equal(validUser.id)
          done()
        })
    })
  })
  describe('GET /', () => {
    it('should get a single user record', (done) => {
      chai.request(app)
        .get(`/users/${validUser.id}`)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          expect(res.body.user._id).to.equal(validUser.id)
          done()
        })
    })
    it('should not get a single user record', (done) => {
      const id = 3
      chai.request(app)
        .get(`/users/${id}`)
        .end((err, res) => {
          res.should.have.status(404)
          done()
        })
    })
  })
})
