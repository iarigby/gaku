'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const chai = require('chai')
const expect = chai.expect

const Profile = require('../models/spotify/profile').model

const profileFactory = require('./factories/profile.factory')
const userFactory = require('./factories/user.factory')
const partyFactory = require('./factories/party.factory')
const queueEntryFactory = require('./factories/queue.entry.factory')
const trackFactory = require('./factories/spotify/track.factory')
const playlistFactory = require('./factories/spotify/playlist.factory')


describe('Database Tests', function() {
  before(function (done) {
    mongoose.connect('mongodb://localhost/testDatabase', {useNewUrlParser: true})
    const db = mongoose.connection
    db.on('error', console.error.bind(console, 'Connection error'))
    db.once('open', function() {
      console.log('We are connected to test database!')
      done()
    })
  })
  describe('Test Database', function() {
    it('New spotify Profile object saved to database', function(done) {
      var validProfile = profileFactory.validProfile()
      validProfile.save(done)
    })
    it('New User object saved to database', function(done) {
      var validUser = userFactory.validUser()
      validUser.save(done)
    })
    it('New Party object saved to database', function(done) {
      var validParty = partyFactory.validParty()
      validParty.save(done)
    })
    it('New QueueEntry object saved to database', function(done) {
      var validQueueEntry = queueEntryFactory.validQueueEntry()
      validQueueEntry.save(done)
    })
    it('New Track object saved to database', function(done) {
      var validTrack = trackFactory.validTrack()
      validTrack.save(done)
    })
    it('New Playlist object saved to database', function(done) {
      var validPlaylist = playlistFactory.validPlaylist()
      validPlaylist.save(done)
    })

    it('Do not save incorrect format to database', function(done) {
      //Attempt to save with wrong info. An error should trigger
      var invalidProfile = profileFactory.invalidProfile()

      invalidProfile.save(err => {
        if (err) {
          return done()
        }
        throw new Error('Should generate error!')
      })
    })
    it('Should retrieve data from test database', function(done) {
      //Look up the user profile object previously saved.
      Profile.find({display_name: 'Lilla Namo'}, (err, name) => {
        if (err) {
          throw err
        }
        if (name.length === 0) {
          throw new Error('No data!')
        }
        done()
      })
    })
  })

  //Drop the database and close the connection
  after(function(done){
    mongoose.connection.db.dropDatabase(function(){
      mongoose.connection.close(done)
    })
  })
})
