/**
 * server application that receives requests
 */

const app = require('./app')

// defaults to 3000 in case the environment variable is not set
// for example in dev environment
const port = process.env.PORT || 3000
app.listen(port, () => console.log(`server started on port ${port}`))
