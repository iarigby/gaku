const express = require('express')
const app = express()
const morgan = require('morgan')
const bodyParser = require('body-parser')
const expressSwagger = require('express-swagger-generator')(app)
const options = require('./config/swagger-options')
expressSwagger(options)

require('dotenv').config()
require('./config/db')

const healthCheckRoute = require('./api/routes/healthCheck')
const usersRoute = require('./api/routes/users')
const partiesRoute = require('./api/routes/parties')

// log all incoming requests
app.use(morgan('dev'))

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

app.use(corsHandling)

/**
 * ROUTES
 * all the root endpoints (ex: party, user) will be listed here
 */
app.use('/healthCheck', healthCheckRoute)
app.use('/users', usersRoute)
app.use('/parties', partiesRoute)

// if none of the existing app.use matched the request, this one is called
app.use((req, res, next) => {
  const error = new Error('Not found')
  error.status(404)
  next(error)
})

// error handling defaults to 500
app.use((error, req, res, next) => {
  res.status(error.status || 500)
  res.json({
    error: {
      message: error.message
    }
  })
})

function corsHandling(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*'),
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-requested-With, Content-Type, Accept, Authorization'
  )
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET')
    return res.status(200).json({})
  }
  next()
}
module.exports = app
